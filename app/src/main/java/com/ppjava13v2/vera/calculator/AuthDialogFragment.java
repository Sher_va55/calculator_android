package com.ppjava13v2.vera.calculator;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.*;

/**
 * Creates dialog window for logging users
 */

public class AuthDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    public interface AuthDialogListener  {

        void finishApp ();
        void checkPassword (View view);

    }

    private AuthDialogListener mAuthDialogListener;
    private View authView;

    public AuthDialogFragment() {
    }

    @SuppressLint("ValidFragment")
    public AuthDialogFragment(AuthDialogListener mAuthDialogListener) {
        this.mAuthDialogListener = mAuthDialogListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        authView =inflater.inflate (R.layout.auth_layout,null);

        AlertDialog.Builder builder = new AlertDialog.Builder (getActivity());
        builder.setTitle("Welcome Calculator")
                .setCancelable(false)
                .setView(authView)
                .setPositiveButton("Yes",this)
                .setNegativeButton("No",this);


        return builder.create ();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {

            case DialogInterface.BUTTON_POSITIVE:
                mAuthDialogListener.checkPassword(authView);
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                mAuthDialogListener.finishApp();
                break;

        }
    }
}
