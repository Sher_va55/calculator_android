package com.ppjava13v2.vera.calculator;

import android.app.*;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.ppjava13v2.vera.calculator.Services.*;

import java.math.BigDecimal;
import java.util.Date;


public class MainActivity extends Activity implements AuthDialogFragment.AuthDialogListener{

    private static final long PAUSE_TIME = 8*1000;
    private static final String AUTH_STATE="authState";
    public static final int EXIT=R.id.exit;
    public static final int HELP=R.id.help;
    public static final int ABOUT=R.id.about;

    private Calculations calculations;
    private BigDecimal result;
    private CheckPassword checkPasswordService;

    private Date mResizeButtonPressedTime;
    private Date mReNewAppTime;

    private boolean mIsAuth;


    public MainActivity() {

        calculations= new Calculations();
        result=new BigDecimal(0);
        checkPasswordService=new CheckPassword ();
    }

    private View.OnClickListener makeExspression (final Button btn,final TextView tw) {
        View.OnClickListener onClickListener = new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                if ("C".equals (btn.getText())) {
                    tw.setText("");

                }
                else if ("=".equals(btn.getText())) {
                    result=calculations.calculate();
                    tw.setText (result.toString());

                }
                else {
                    if (tw.getText().equals(result.toString())) {
                        tw.setText("");

                    }
                    tw.setText(tw.getText() + " " + btn.getText());
                }

            }
        };
        return onClickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null && savedInstanceState.containsKey(AUTH_STATE)) {
            mIsAuth=savedInstanceState.getBoolean(AUTH_STATE);
        }

        setContentView(R.layout.activity_main);


        final TextView display = (TextView) findViewById(R.id.result);

        final Button num0 = (Button) findViewById(R.id.num0);
        final Button num1 = (Button) findViewById(R.id.num1);
        final Button num2 = (Button) findViewById(R.id.num2);
        final Button num3 = (Button) findViewById(R.id.num3);
        final Button num4 = (Button) findViewById(R.id.num4);
        final Button num5 = (Button) findViewById(R.id.num5);
        final Button num6 = (Button) findViewById(R.id.num6);
        final Button num7 = (Button) findViewById(R.id.num7);
        final Button num8 = (Button) findViewById(R.id.num8);
        final Button num9 = (Button) findViewById(R.id.num9);

        final Button plus = (Button) findViewById(R.id.plus);
        final Button minus = (Button) findViewById(R.id.minus);
        final Button divide = (Button) findViewById(R.id.divide);
        final Button multi = (Button) findViewById(R.id.multi);
        final Button clear = (Button) findViewById(R.id.clear);
        final Button equal = (Button) findViewById(R.id.equal);
        final Button comma=(Button) findViewById(R.id.comma);



        num0.setOnClickListener(makeExspression(num0,display));
        num1.setOnClickListener(makeExspression(num1,display));
        num2.setOnClickListener(makeExspression(num2,display));
        num3.setOnClickListener(makeExspression(num3,display));
        num4.setOnClickListener(makeExspression(num4,display));
        num5.setOnClickListener(makeExspression(num5,display));
        num6.setOnClickListener(makeExspression(num6,display));
        num7.setOnClickListener(makeExspression(num7,display));
        num8.setOnClickListener(makeExspression(num8,display));
        num9.setOnClickListener(makeExspression(num9,display));

        plus.setOnClickListener(makeExspression (plus,display));
        minus.setOnClickListener(makeExspression (minus,display));
        divide.setOnClickListener(makeExspression (divide,display));
        multi.setOnClickListener(makeExspression (multi,display));
        comma.setOnClickListener(makeExspression (comma,display));

        clear.setOnClickListener (makeExspression (clear,display));

        equal.setOnClickListener(makeExspression(equal, display));

        registerForContextMenu (display);

    }

    @Override
    protected void onResume() {

        super.onResume();
        mReNewAppTime=new Date ();

        if (null!=mResizeButtonPressedTime) {
            if (requiredTimeReNewAuth ()) {
                mIsAuth=false;
            }
        }
        if (!mIsAuth) {
            showAuthDialog ();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(AUTH_STATE, mIsAuth);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mIsAuth=savedInstanceState.getBoolean(AUTH_STATE);
        Log.wtf("Lifecycle", "OnRestoreInstanceState");
    }

    @Override
    protected void onPause() {
        super.onPause();
        mResizeButtonPressedTime=new Date ();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater=getMenuInflater ();
        menuInflater.inflate(R.menu.menu_main, menu);


        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case EXIT:
                finishApp();
                break;
            case HELP:
                Toast.makeText(this,item.getTitle()+ " selected",Toast.LENGTH_SHORT).show();
                break;
            case ABOUT:
                Toast.makeText (this, item.getTitle()+ " selected",Toast.LENGTH_SHORT).show();

        }
        return false;

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu_context,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        Toast.makeText (this, item.getTitle()+ " selected",Toast.LENGTH_SHORT).show();
        return true;
    }

    void showAuthDialog () {

        DialogFragment authDialog = new AuthDialogFragment(this);
        authDialog.show (getFragmentManager (),"authTag");

    }

    private boolean requiredTimeReNewAuth() {

        return mReNewAppTime.getTime() - mResizeButtonPressedTime.getTime() > PAUSE_TIME;
    }


    @Override
    public void finishApp() {
        finish ();
    }

    @Override
    public void checkPassword(View view) {

        EditText login= (EditText) view.findViewById(R.id.user);
        EditText password= (EditText) view.findViewById(R.id.password);
        mIsAuth=checkPasswordService.checkPassword(login.getText().toString(),password.getText().toString());

        if (!mIsAuth) {
            showAuthDialog ();
        }
        else {
            mIsAuth=true;
        }

    }
}
