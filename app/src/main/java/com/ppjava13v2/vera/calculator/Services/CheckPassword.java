package com.ppjava13v2.vera.calculator.Services;


import com.ppjava13v2.vera.calculator.Entities.Users;

import java.util.*;


/**
 * Creates list of users and checks login and password from DialogFragment
 */
public class CheckPassword {

    private List<Users> users;
    boolean equals;

    public CheckPassword() {

        users = new ArrayList<>();
        users.add (new Users ("Vera","123456"));
        users.add (new Users ("Admin","qwerty"));
        users.add (new Users ("Test","Test"));
    }

    public boolean checkPassword (String login, String password) {

        for (Users user:users) {
            if (login.equals(user.getLogin())&&password.equals(user.getPassword())) {
                equals= true;
            }
        }

        return equals;
    }
}

